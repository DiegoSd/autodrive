1. write the test for more advanced Sensors. try to implement the sensors 
specified in the practice, inheriting from BaseSensor and BaseVirtualSensor.
    -> if it doesn't work, ditch the branch and reimplement them from the
       mockup implementation in Sensor_. 

2. merge to develop and merge to master as v1.0.

3. *try* to convert the Message types from Strings to enumeration. maybe this
already happened in step 1.

4. write the tests for the standalone AutoDrive: a class wrapping instances of
all the previously described Sensors. in the test, assert encapsulating
functionality for the Sensors activation (maybe using the Runnable interface).

5. finally write tests for interacting AutoDrives: first, only two of them
cohabiting the same Bus and testing that the one running behind adjusts its
speed to match that of the one running first.
    -> to do this, the best way would be to implement a new testing class that
       in principle inherits the behavior of a pure AutoDrive, and override
       the logic to adjust it to the new needs.

6. refactor every possible class in the final production code, as well as
rename the last versions of the classes to its *final* production name.

7(itff). add the Intersection resources embedded in the Bus (maybe by this
time it would be wise to rename it to Road) as well as a new type of Sensor
that sends a request for said resources. change the logic of the AutoDrives to
make it so that all the cars step into the resources as soon as possible.
    -> plot twist: Intersections also have VirtualSensors. these Sensors would
       record the requests from the AutoDrives and echo them: in this system,
       an AutoDrive who wanted to enter an Intersection must subscribe to the
       Intersections publications in order to know where all of the other
       AutoDrives stand and in which order the requests were issued, so that
       each one can act on that logic.
        -> plot twist twist: maybe all of this can be isolated within the
           AutoDrives.
