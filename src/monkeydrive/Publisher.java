package monkeydrive;

public interface Publisher {
    void associate(Bus bus);
    void publish();
}
