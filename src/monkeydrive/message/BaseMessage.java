package monkeydrive.message;

import monkeydrive.Message;

public class BaseMessage implements Message {
    private String data;

    public BaseMessage(String data) {
        this.data = data;
    }

    @Override
    public String type() {
        return "base";
    }

    @Override
    public String data() {
        return String.valueOf(data);
    }
}

