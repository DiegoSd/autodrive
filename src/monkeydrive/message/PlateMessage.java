package monkeydrive.message;

public class PlateMessage extends BaseMessage {

    public PlateMessage(String data) {
        super(data);
    }

    @Override
    public String type() {
        return "plate";
    }
}
