package monkeydrive.message;


public class CarAheadSpeedMessage extends BaseMessage {

    public CarAheadSpeedMessage(double data) {
        super(String.valueOf(data));
    }

    @Override
    public String type() {
        return "car-ahead-speed";
    }
}
