package monkeydrive.message;

public class DistanceMessage extends BaseMessage {

    public DistanceMessage(double data) {
        super(String.valueOf(data));
    }

    @Override
    public String type() {
        return "distance";
    }
}
