package monkeydrive.message;

public class SpeedMessage extends BaseMessage {

    public SpeedMessage(double data) {
        super(String.valueOf(data));
    }

    @Override
    public String type() {
        return "speed";
    }
}
