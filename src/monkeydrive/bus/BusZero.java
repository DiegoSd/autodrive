package monkeydrive.bus;

import monkeydrive.Bus;
import monkeydrive.Message;
import monkeydrive.Subscriber;
import monkeydrive.Subscription;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class BusZero implements Bus {

    private Map<String,Set<Subscriber>> subscriberMap = new HashMap<>();

    @Override
    public Subscription register(Subscriber subscriber) {
        return types -> {
            for(String type : types) {
                subscribersOf(type).add(subscriber);
            }
        };
    }

    @Override
    public void send(Message message) {
        if (subscriberMap.containsKey(message.type()))
            subscriberMap.get(message.type()).forEach(s -> s.receive(message));
    }

    private Set<Subscriber> subscribersOf(String type) {
        if (subscriberMap.containsKey(type)) return subscriberMap.get(type);
        Set<Subscriber> newSet = new HashSet<>();
        subscriberMap.put(type, newSet);
        return newSet;
    }

}
