package monkeydrive;

public interface Subscriber {
    void receive(Message message);
}
