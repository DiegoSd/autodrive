package monkeydrive;

public interface Message {
    String type();
    String data();
}
