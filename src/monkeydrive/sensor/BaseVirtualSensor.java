package monkeydrive.sensor;

import monkeydrive.Message;
import monkeydrive.VirtualSensor;
import monkeydrive.message.BaseMessage;

public class BaseVirtualSensor extends BaseSensor implements VirtualSensor {

    @Override
    protected Message generateMessage() {
        return new BaseMessage("virtual-sensor");
    }

    @Override
    public void receive(Message message) {
        publish();
    }
}
