package monkeydrive.sensor;

import monkeydrive.Message;
import monkeydrive.message.PlateMessage;

public class PlateSensor extends BaseSensor {

    private String perceivedPlate = null;

    @Override
    protected Message generateMessage() {
        return new PlateMessage(perceivedPlate);
    }

    public void updatePerceivedPlate(String plate){
        perceivedPlate = plate;
    }
}
