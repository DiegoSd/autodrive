package monkeydrive.sensor;

import monkeydrive.Message;
import monkeydrive.message.CarAheadSpeedMessage;

import java.time.Instant;

import static java.lang.Double.NaN;
import static java.lang.Double.isNaN;

public class CarAheadSpeedSensor extends BaseVirtualSensor {

    private final int NEW = 1;
    private final int PREV = 0;

    private final long MAXINTERVAL = 30;
    private final double MININTERVAL = 0.1;

    private boolean newCar = true;

    Instant lastUpdate = Instant.MIN;

    private double[] speeds = {NaN, NaN};
    private double[] distances = {NaN, NaN};
    private String plate = "empty";

    private double lastRegisteredCas = NaN;

    @Override
    public void receive(Message message) {
        if (message.type() == "speed") updateSpeed(message.data());
        else if (message.type() == "plate") updatePlate(message.data());
        else updateDistance(message.data());
        publish();
    }

    @Override
    protected Message generateMessage() {
        double interval = getInterval();

        if (interval < MININTERVAL) return new CarAheadSpeedMessage(lastRegisteredCas);
        if (newCar | cantCalculate()) return new CarAheadSpeedMessage(NaN);

        return new CarAheadSpeedMessage(calculateCas(interval));
    }

    private double calculateCas(double interval) {
        if (distances[NEW] - distances[PREV] == 0) return lastRegisteredCas = speeds[NEW];
        else return lastRegisteredCas = otherVehicleSpeed(traversed(speeds[NEW], acceleration(interval), interval),
                distanceBetweenDrop(), interval);
    }


    private boolean cantCalculate() {
        return isNaN(distances[PREV]) || isNaN(speeds[PREV]);
    }

    private void updatePlate(String data) {
        String prev = plate;
        plate = data;
        newCar = (prev == "empty" || prev != data);
    }

    private void updateSpeed(String data) {
        speeds[PREV] = speeds[NEW];
        speeds[NEW] = Double.parseDouble(data);
    }

    private void updateDistance(String data) {
        distances[PREV] = distances[NEW];
        distances[NEW] = Double.parseDouble(data);
    }

    private double acceleration(double interval) {
        return (speeds[NEW] - speeds[PREV]) / interval;

    }

    private double getInterval() {
        Instant now = Instant.now();

        if(lastUpdate == Instant.MIN) {
            lastUpdate = now;
            return MAXINTERVAL + 1;
        }

        long interval = now.toEpochMilli() - lastUpdate.toEpochMilli();
        lastUpdate = now;

        return interval/1000.0;
    }

    private double distanceBetweenDrop() {
        return distances[NEW] - distances[PREV];
    }

    private static double traversed(double initialSpeed, double acceleration, double interval) {
        return initialSpeed * interval + 0.5 * acceleration * (interval * interval);
    }

    private static double otherVehicleSpeed(double traversedDistance, double distanceBetweenDrop, double interval) {
        return (traversedDistance + distanceBetweenDrop) / interval;
    }
}
