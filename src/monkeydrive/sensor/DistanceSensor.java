package monkeydrive.sensor;

import monkeydrive.Message;
import monkeydrive.message.DistanceMessage;

public class DistanceSensor extends BaseSensor {

    private double perceivedDistance = Double.NaN;

    @Override
    protected Message generateMessage() {
        return new DistanceMessage(perceivedDistance);
    }

    public void updatePerceivedDistance(double distance){
        perceivedDistance = distance;
    }
}
