package monkeydrive.sensor;

import monkeydrive.Message;
import monkeydrive.message.SpeedMessage;

public class SpeedSensor extends BaseSensor {

    private double perceivedSpeed = Double.NaN;

    @Override
    protected Message generateMessage() {
        return new SpeedMessage(perceivedSpeed);
    }

    public void updatePerceivedSpeed(double speed){
        perceivedSpeed = speed;
    }
}
