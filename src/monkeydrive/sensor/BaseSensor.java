package monkeydrive.sensor;

import monkeydrive.Bus;
import monkeydrive.Message;
import monkeydrive.Sensor;
import monkeydrive.message.BaseMessage;

public class BaseSensor implements Sensor {

    private Bus associatedBus;

    @Override
    public void associate(Bus bus) {
        associatedBus = bus;
    }

    @Override
    public void publish() {
        associatedBus.send(generateMessage());
    }

    protected Message generateMessage() {
        return new BaseMessage("sensor");
    }
}
