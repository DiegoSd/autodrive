package monkeydrive;

public interface AutoDrive extends Runnable {

    double maxAcceleration = 5;
    double maxBreakDeceleration = -8;

    void startAutoDrive() throws InterruptedException;
    void stopAutoDrive();

    void injectSensor(Sensor sensor);

    void connectTo(Bus outerBus);
}
