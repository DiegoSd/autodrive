package monkeydrive;

public interface Subscription {
    void to(String... types);
}
