package monkeydrive;

public interface Bus {
    Subscription register(Subscriber subscriber);
    void send(Message message);
}
