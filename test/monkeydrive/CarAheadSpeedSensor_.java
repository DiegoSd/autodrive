package monkeydrive;

import monkeydrive.bus.BusZero;
import monkeydrive.sensor.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.List;

import static java.lang.Double.NaN;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.mockito.Mockito.*;

public class CarAheadSpeedSensor_ {

    private Bus bus;

    private SpeedSensor speedSensor;
    private DistanceSensor distanceSensor;
    private PlateSensor plateSensor;

    private CarAheadSpeedSensor casSensor = new CarAheadSpeedSensor();

    private String[] testingPlates = {"0000BBB","1234BCD","9999ZZZ","error"};

    @Before
    public void setUp() {
        bus = new BusZero();
        speedSensor = new SpeedSensor();
        distanceSensor = new DistanceSensor();
        plateSensor = new PlateSensor();

        speedSensor.associate(bus);
        distanceSensor.associate(bus);
        plateSensor.associate(bus);

        bus.register(casSensor).to("speed","distance","plate");
        casSensor.associate(bus);
    }

    @Test
    public void should_register_same_linear_velocity() throws InterruptedException {
        Subscriber subscriber = mock(Subscriber.class);
        bus.register(subscriber).to("car-ahead-speed");

        String plateOfCarAhead = "1234BCD";
        double speed = 16;
        double distance = 50;

        for(int i=0; i<5; i++) {
            plateSensor.updatePerceivedPlate(plateOfCarAhead);
            plateSensor.publish();
            speedSensor.updatePerceivedSpeed(speed);
            speedSensor.publish();
            distanceSensor.updatePerceivedDistance(distance);
            distanceSensor.publish();
            Thread.sleep(500);
        }

        ArgumentCaptor<Message> argumentCaptor = ArgumentCaptor.forClass(Message.class);
        verify(subscriber,times(15)).receive(argumentCaptor.capture());

        List<Message> capturedMessages = argumentCaptor.getAllValues();

        for(int i=0; i<15; i++) {
            if (i < 6)
                assertThat(capturedMessages.get(i).data(), equalTo(String.valueOf(NaN)));
            else
                assertThat(Double.parseDouble(capturedMessages.get(i).data()), closeTo(speed,0.1));
        }
    }

    @Test
    public void should_register_higher_linear_velocity() throws InterruptedException {
        Subscriber subscriber = mock(Subscriber.class);
        bus.register(subscriber).to("car-ahead-speed");

        String plateOfCarAhead = "1234BCD";

        double speed = 16;
        double[] distancesBetween = {25.0,25.5,26.0,26.5,27.0};

        double expectedCarAheadSpeed = speed + 1;

        for(int i=0; i<5; i++) {
            plateSensor.updatePerceivedPlate(plateOfCarAhead);
            plateSensor.publish();
            speedSensor.updatePerceivedSpeed(speed);
            speedSensor.publish();
            distanceSensor.updatePerceivedDistance(distancesBetween[i]);
            distanceSensor.publish();
            Thread.sleep(500);
        }

        ArgumentCaptor<Message> argumentCaptor = ArgumentCaptor.forClass(Message.class);
        verify(subscriber,times(15)).receive(argumentCaptor.capture());

        List<Message> capturedMessages = argumentCaptor.getAllValues();

        for(int i=0; i<15; i++) {
            if (i < 6)
                assertThat(capturedMessages.get(i).data(), equalTo(String.valueOf(NaN)));
            else
                assertThat(Double.parseDouble(capturedMessages.get(i).data()), closeTo(expectedCarAheadSpeed,0.1));
        }
    }

    @Test
    public void should_register_lower_linear_velocity() throws InterruptedException {
        Subscriber subscriber = mock(Subscriber.class);
        bus.register(subscriber).to("car-ahead-speed");

        String plateOfCarAhead = "1234BCD";

        double speed = 16;
        double[] distancesBetween = {25.0,24.5,24.0,23.5,23.0};

        double expectedCarAheadSpeed = speed - 1;

        for(int i=0; i<5; i++) {
            plateSensor.updatePerceivedPlate(plateOfCarAhead);
            plateSensor.publish();
            speedSensor.updatePerceivedSpeed(speed);
            speedSensor.publish();
            distanceSensor.updatePerceivedDistance(distancesBetween[i]);
            distanceSensor.publish();
            Thread.sleep(500);
        }

        ArgumentCaptor<Message> argumentCaptor = ArgumentCaptor.forClass(Message.class);
        verify(subscriber,times(15)).receive(argumentCaptor.capture());

        List<Message> capturedMessages = argumentCaptor.getAllValues();

        for(int i=0; i<15; i++) {
            if (i < 6)
                assertThat(capturedMessages.get(i).data(), equalTo(String.valueOf(NaN)));
            else
                assertThat(Double.parseDouble(capturedMessages.get(i).data()), closeTo(expectedCarAheadSpeed,0.1));
        }
    }

    @Test
    public void should_keep_track_of_changes_in_car_speed() throws InterruptedException {
        Subscriber subscriber = mock(Subscriber.class);
        bus.register(subscriber).to("car-ahead-speed");

        String plateOfCarAhead = "1234BCD";
        double expectedCarAheadSpeed = 16.0;

        double[] speeds    = { 16.00, 16.00, 16.00, 16.00,  16.50, 17.00, 17.00, 17.00};
        double[] distances = { 30.00, 30.00, 30.00, 30.00, 29.625, 29.00, 28.50, 28.00};


        for(int i=0; i<8; i++) {
            plateSensor.updatePerceivedPlate(plateOfCarAhead);
            plateSensor.publish();
            speedSensor.updatePerceivedSpeed(speeds[i]);
            speedSensor.publish();
            distanceSensor.updatePerceivedDistance(distances[i]);
            distanceSensor.publish();
            Thread.sleep(500);
        }

        ArgumentCaptor<Message> argumentCaptor = ArgumentCaptor.forClass(Message.class);
        verify(subscriber,times(24)).receive(argumentCaptor.capture());

        List<Message> capturedMessages = argumentCaptor.getAllValues();

        for(int i=0; i<24; i++) {
            if (i < 6)
                assertThat(capturedMessages.get(i).data(), equalTo(String.valueOf(NaN)));
            else
                assertThat(Double.parseDouble(capturedMessages.get(i).data()), closeTo(expectedCarAheadSpeed,0.1));
        }
    }

    @Test
    public void should_keep_track_of_changes_in_car_ahead_speed() throws InterruptedException {
        Subscriber subscriber = mock(Subscriber.class);
        bus.register(subscriber).to("car-ahead-speed");

        String plateOfCarAhead = "1234BCD";
        double[] expectedCarAheadSpeeds = { 20.0, 20.0, 19.5, 19.0, 18.5, 18.0, 17.5, 17.0 };

        double speed = 16.00;
        double[] distances = { 50.0, 52.008, 53.633, 55.008, 56.133, 57.01, 57.635, 58.00975 };

        for(int i=0; i<8; i++) {
            plateSensor.updatePerceivedPlate(plateOfCarAhead);
            plateSensor.publish();
            speedSensor.updatePerceivedSpeed(speed);
            speedSensor.publish();
            distanceSensor.updatePerceivedDistance(distances[i]);
            distanceSensor.publish();
            Thread.sleep(500);
        }

        ArgumentCaptor<Message> argumentCaptor = ArgumentCaptor.forClass(Message.class);
        verify(subscriber,times(24)).receive(argumentCaptor.capture());

        List<Message> capturedMessages = argumentCaptor.getAllValues();

        for(int i=0; i<21; i++) {
            if (i < 6)
                assertThat(capturedMessages.get(i).data(), equalTo(String.valueOf(NaN)));
            else
                assertThat(Double.parseDouble(capturedMessages.get(i).data()), closeTo(expectedCarAheadSpeeds[i/3],0.75));
        }

    }

    @Test
    public void should_keep_track_of_changes_in_car_ahead_speed_faster() throws InterruptedException {
        Subscriber subscriber = mock(Subscriber.class);
        bus.register(subscriber).to("car-ahead-speed");

        String plateOfCarAhead = "1234BCD";
        double[] expectedCarAheadSpeeds = {20.0, 20.0, 19.5, 19.0, 18.5, 18.0, 17.5, 17.0};

        double speed = 16.00;
        double[] distances = { 50.0, 51.024, 51.836, 52.524, 53.09, 53.53, 53.84, 54.027 };

        for (int i = 0; i < 8; i++) {
            plateSensor.updatePerceivedPlate(plateOfCarAhead);
            plateSensor.publish();
            speedSensor.updatePerceivedSpeed(speed);
            speedSensor.publish();
            distanceSensor.updatePerceivedDistance(distances[i]);
            distanceSensor.publish();
            Thread.sleep(250);
        }

        ArgumentCaptor<Message> argumentCaptor = ArgumentCaptor.forClass(Message.class);
        verify(subscriber, times(24)).receive(argumentCaptor.capture());

        List<Message> capturedMessages = argumentCaptor.getAllValues();

        for (int i = 0; i < 21; i++) {
            if (i < 6)
                assertThat(capturedMessages.get(i).data(), equalTo(String.valueOf(NaN)));
            else
                assertThat(Double.parseDouble(capturedMessages.get(i).data()), closeTo(expectedCarAheadSpeeds[i / 3], 0.7));
        }

    }
}
