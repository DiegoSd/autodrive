package monkeydrive;

import monkeydrive.bus.BusZero;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class Bus_ {
    private Bus bus;

    @Before
    public void setUp() {
        bus = new BusZero();
    }

    @Test
    public void subscriber_should_receive_message() {
        Subscriber subscriber = mock(Subscriber.class);
        Message message = messageOfType("all");

        bus.register(subscriber).to("all");
        bus.send(message);
        bus.send(message);
        bus.send(message);

        verify(subscriber,times(3)).receive(any());
    }

    @Test
    public void should_register_multiple_subscribers() {
        Subscriber subscriber1 = mock(Subscriber.class);
        Subscriber subscriber2 = mock(Subscriber.class);
        Subscriber subscriber3 = mock(Subscriber.class);

        bus.register(subscriber1).to("all");
        bus.register(subscriber2).to("all");
        bus.register(subscriber3).to("all");

        bus.send(messageOfType("all"));

        verify(subscriber1,times(1)).receive(any());
        verify(subscriber2,times(1)).receive(any());
        verify(subscriber3,times(1)).receive(any());
    }

    @Test
    public void should_filter_messages_to_subscribers() {
        Subscriber subscriberA = mock(Subscriber.class);
        Subscriber subscriberB = mock(Subscriber.class);
        Subscriber subscriberC = mock(Subscriber.class);

        bus.register(subscriberA).to("foo");
        bus.register(subscriberB).to("bar");
        bus.register(subscriberC).to("foo", "bar");

        bus.send(messageOfType("foo"));
        bus.send(messageOfType("bar"));

        verify(subscriberA,times(1)).receive(any());
        verify(subscriberB,times(1)).receive(any());
        verify(subscriberC,times(2)).receive(any());
    }

    private Message messageOfType(String type) {
        Message message = mock(Message.class);
        when(message.type()).thenReturn(type);
        return message;
    }

}
