package monkeydrive;

import monkeydrive.sensor.PlateSensor;
import monkeydrive.bus.BusZero;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class PlateSensor_ {

    private Bus bus;
    private PlateSensor plateSensor;

    private String[] testingPlates = {"0000BBB","1234BCD","9999ZZZ","error"};

    @Before
    public void setUp() {
        bus = new BusZero();
        plateSensor = new PlateSensor();

        plateSensor.associate(bus);
    }

    @Test
    public void should_send_correct_data() {
        Subscriber subscriber = mock(Subscriber.class);

        bus.register(subscriber).to("plate");

        plateSensor.updatePerceivedPlate(testingPlates[0]);
        plateSensor.publish();

        ArgumentCaptor<Message> argumentCaptor = ArgumentCaptor.forClass(Message.class);
        verify(subscriber).receive(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue().data(), equalTo("0000BBB"));
    }

    @Test
    public void should_keep_track_of_changes() {
        Subscriber subscriber = mock(Subscriber.class);

        bus.register(subscriber).to("plate");

        plateSensor.updatePerceivedPlate(testingPlates[1]);
        plateSensor.publish();
        plateSensor.updatePerceivedPlate(testingPlates[2]);
        plateSensor.publish();
        plateSensor.updatePerceivedPlate(testingPlates[3]);
        plateSensor.publish();

        ArgumentCaptor<Message> messageCaptor = ArgumentCaptor.forClass(Message.class);
        verify(subscriber,times(3)).receive(messageCaptor.capture());

        List<Message> capturedMessages = messageCaptor.getAllValues();

        assertThat(capturedMessages.get(0).data(), equalTo("1234BCD"));
        assertThat(capturedMessages.get(1).data(), equalTo("9999ZZZ"));
        assertThat(capturedMessages.get(2).data(), equalTo("error"));
    }

}
