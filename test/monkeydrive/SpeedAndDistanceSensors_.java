package monkeydrive;

import monkeydrive.bus.BusZero;
import monkeydrive.sensor.DistanceSensor;
import monkeydrive.sensor.SpeedSensor;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class SpeedAndDistanceSensors_ {

    private Bus bus;

    private SpeedSensor speedSensor;
    private DistanceSensor distanceSensor;

    private double[] testingSpeeds = {1.0,2.5,0.025,9999.99};
    private double[] testingDistances = {10.5,50.3,2.1,9999.99};

    @Before
    public void setUp() {
        bus = new BusZero();
        speedSensor = new SpeedSensor();
        distanceSensor = new DistanceSensor();

        speedSensor.associate(bus);
        distanceSensor.associate(bus);
    }

    @Test
    public void should_work_as_expected() {
        Subscriber subscriber = mock(Subscriber.class);

        bus.register(subscriber).to("speed","distance");

        speedSensor.updatePerceivedSpeed(testingSpeeds[0]);
        speedSensor.publish();
        distanceSensor.updatePerceivedDistance(testingDistances[0]);
        distanceSensor.publish();

        ArgumentCaptor<Message> argumentCaptor = ArgumentCaptor.forClass(Message.class);
        verify(subscriber,times(2)).receive(argumentCaptor.capture());

        List<Message> capturedMessages = argumentCaptor.getAllValues();

        assertThat(capturedMessages.get(0).data(), equalTo(String.valueOf(testingSpeeds[0])));
        assertThat(capturedMessages.get(1).data(), equalTo(String.valueOf(testingDistances[0])));
    }
}
