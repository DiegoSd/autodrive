package monkeydrive;

import com.google.common.collect.Lists;
import monkeydrive.bus.BusZero;
import monkeydrive.message.CarAheadSpeedMessage;
import monkeydrive.message.DistanceMessage;
import monkeydrive.sensor.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import utils.CasSensorOverride;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.lang.Double.NaN;
import static java.lang.Double.isNaN;
import static java.lang.Double.parseDouble;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.mockito.Mockito.*;

public class AutoDrive_ {

    private static final double speedLimit = 33.33;
    private static final int definedInterval = 100;

    private Bus outerBus;
    private DistanceSensor distanceSensor;
    private SpeedSensor speedSensor;
    private PlateSensor plateSensor;
    private CarAheadSpeedSensor casSensor;

    private CasSensorOverride casSensorOverride;

    private AutoDrive autoDrive;

    @Before
    public void setUp() {
        outerBus = new BusZero();
        distanceSensor = new DistanceSensor();
        speedSensor = new SpeedSensor();
        plateSensor = new PlateSensor();

        casSensor = new CarAheadSpeedSensor();
        casSensorOverride = new CasSensorOverride();

        autoDrive = new AutoDrive() {

            @Override
            public void run() {
                try { startAutoDrive(); }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            private static final long interval = definedInterval;
            private static final double intervalInSeconds = interval/1000.0;
            private static final double maxSecurityDistance = 70;

            private double speed = 0;
            private double detectedCarAheadSpeed = NaN;
            private double distanceToCarAhead = NaN;
            private boolean run = false;

            private Bus innerBus = new BusZero();

            private DistanceSensor innerDistanceSensor;
            private SpeedSensor innerSpeedSensor;
            private PlateSensor innerPlateSensor;
            private CarAheadSpeedSensor innerCasSensor;

            private Set<Sensor> sensors = new HashSet<>(
                    Arrays.asList());

            private BaseVirtualSensor notifier = new BaseVirtualSensor() {
                private Message stagedMessage;

                @Override
                protected Message generateMessage() {
                    return stagedMessage;
                }

                @Override
                public void receive(Message message) {
                    stagedMessage = message;
                    if(message instanceof DistanceMessage) distanceToCarAhead = parseDouble(message.data());
                    if(message instanceof CarAheadSpeedMessage) detectedCarAheadSpeed = parseDouble(message.data());
                    publish();
                }
            };

            private Subscriber innerUpdater = message -> {
                if(message instanceof DistanceMessage) distanceToCarAhead = parseDouble(message.data());
                if(message instanceof CarAheadSpeedMessage) detectedCarAheadSpeed = parseDouble(message.data());
            };



            @Override
            public void startAutoDrive() throws InterruptedException {
                innerBus.register(notifier).to("speed","distance");
                innerBus.register(innerUpdater).to("distance","car-ahead-speed");
                //innerBus.register(innerCasSensor).to("plate","speed","distance");

                this.sensors.addAll(Arrays.asList(innerDistanceSensor,innerSpeedSensor,innerPlateSensor,innerCasSensor));
                this.sensors.forEach(sensor -> sensor.associate(innerBus));

                run = true;
                goToDestination();
            }

            @Override
            public void stopAutoDrive() {
                run = false;
            }

            @Override
            public void injectSensor(Sensor sensor) {
                if(sensor instanceof SpeedSensor) innerSpeedSensor = (SpeedSensor) sensor;
                if(sensor instanceof DistanceSensor) innerDistanceSensor = (DistanceSensor) sensor;
                if(sensor instanceof PlateSensor) innerPlateSensor = (PlateSensor) sensor;
                if(sensor instanceof CarAheadSpeedSensor) innerCasSensor = (CarAheadSpeedSensor) sensor;
            }

            private void goToDestination() throws InterruptedException {
                while (run) {
                    if(!carAheadDetected()) {
                        if (speed < speedLimit) accelerate(maxAcceleration);
                    }
                    else {
                        distanceToCarAhead += (detectedCarAheadSpeed-speed)*intervalInSeconds;
                        distanceSensor.updatePerceivedDistance(distanceToCarAhead); distanceSensor.publish();
                        matchCarAheadSpeed();
                    }
                    innerSpeedSensor.updatePerceivedSpeed(speed); innerSpeedSensor.publish();
                    Thread.sleep(interval);
                }
            }

            private boolean carAheadDetected() {
                return !isNaN(distanceToCarAhead) || !isNaN(detectedCarAheadSpeed);
            }

            private void accelerate(double acceleration) {
                speed = (speed + speedGain(acceleration) > speedLimit) ?
                        speedLimit : (acceleration < maxBreakDeceleration) ?
                        speed + speedGain(maxBreakDeceleration) : speed + speedGain(acceleration);
            }

            double speedGain(double acceleration) {
                return acceleration * intervalInSeconds;
            }

            private void matchCarAheadSpeed() {
                if(isNaN(detectedCarAheadSpeed)) accelerate(maxBreakDeceleration);
                else {
                    if (isNaN(distanceToCarAhead)) accelerate((detectedCarAheadSpeed - speed) / timeUntilCollision(maxSecurityDistance));
                    else accelerate((detectedCarAheadSpeed - speed) / timeUntilCollision(distanceToCarAhead - securityDistance(speed)));
                }
            }

            private double securityDistance(double speed) {
                return - (speed*speed) / (2.0 * maxBreakDeceleration);
            }

            private double timeUntilCollision(double distance) {
                return distance / (speed - detectedCarAheadSpeed);
            }

            @Override
            public void connectTo(Bus outerBus) {
                notifier.associate(outerBus);
            }
        };

        autoDrive.connectTo(outerBus);

        autoDrive.injectSensor(speedSensor);
        autoDrive.injectSensor(distanceSensor);
        autoDrive.injectSensor(plateSensor);
        autoDrive.injectSensor(casSensorOverride);
    }

    @Test
    public void should_accelerate_to_speed_limit() throws InterruptedException {
        Subscriber subscriber = mock(Subscriber.class);
        outerBus.register(subscriber).to("speed");

        int seconds = 10;
        int expectedInteractions = 1000 * seconds / definedInterval;

        new Thread(autoDrive).start();

        Thread.sleep(1000*seconds);

        autoDrive.stopAutoDrive();

        ArgumentCaptor<Message> argumentCaptor = ArgumentCaptor.forClass(Message.class);
        verify(subscriber,atMost(expectedInteractions)).receive(argumentCaptor.capture());

        List<Message> capturedMessages = argumentCaptor.getAllValues();

        int actualTimes = capturedMessages.size();

        for (int i=0; i<seconds; i++) {
            if(i * definedInterval < actualTimes)
                assertThat(parseDouble(capturedMessages.get(i * definedInterval).data()),
                        closeTo(speedAtSeconds(i), 0.5));

        }
    }

    @Test
    public void  should_brake_from_max_speed_to_match_slower_car() throws InterruptedException {
        Subscriber subscriber = mock(Subscriber.class);
        outerBus.register(subscriber).to("speed","distance");

        int seconds = 30;
        int expectedInteractions = 1000 * seconds / definedInterval;

        double carAheadSpeed = 20.0;

        double initialDetectedDistance = 80;

        new Thread(autoDrive).start();

        for (int i=0; i<seconds; i++) {
            if(i == 8) {
                System.out.println("-Sending distance update- car ahead at " + initialDetectedDistance + "m "
                        + "at " + carAheadSpeed + "m/s" );
                distanceSensor.updatePerceivedDistance(initialDetectedDistance);
                casSensorOverride.updatePerceivedCas(carAheadSpeed);
                distanceSensor.publish();
                casSensorOverride.publish();
            }
            Thread.sleep(1000);
        }

        autoDrive.stopAutoDrive();

        ArgumentCaptor<Message> argumentCaptor = ArgumentCaptor.forClass(Message.class);
        verify(subscriber,atLeast(expectedInteractions-10)).receive(argumentCaptor.capture());

        List<Message> capturedMessages = argumentCaptor.getAllValues();

        double lastMeasuredDistance = -1;
        double lastMeasuredSpeed = -1;

        for (Message message : Lists.reverse(capturedMessages)) {
            if (message.type().equals("distance"))
                lastMeasuredDistance = Double.parseDouble(message.data());
            if (message.type().equals("speed"))
                lastMeasuredSpeed = Double.parseDouble(message.data());
            if(lastMeasuredDistance > 0 && lastMeasuredSpeed > 0)
                break;
        }

        System.out.println("Last measured distance: " + lastMeasuredDistance);
        System.out.println("Last measured speed: " + lastMeasuredSpeed);

        assertThat(lastMeasuredDistance,greaterThan(25.0));
        assertThat(lastMeasuredSpeed,closeTo(carAheadSpeed,0.5));
    }

    @Test
    public void should_brake_from_max_speed_to_match_slower_car_extreme() throws InterruptedException {
        Subscriber subscriber = mock(Subscriber.class);
        outerBus.register(subscriber).to("speed","distance");

        int seconds = 30;
        int expectedInteractions = 1000 * seconds / definedInterval;

        double carAheadSpeed = 16.0;

        double initialDetectedDistance = 75;

        new Thread(autoDrive).start();

        for (int i=0; i<seconds; i++) {
            if(i == 8) {
                System.out.println("-Sending distance update- car ahead at " + initialDetectedDistance + "m "
                        + "at " + carAheadSpeed + "m/s" );
                distanceSensor.updatePerceivedDistance(initialDetectedDistance);
                casSensorOverride.updatePerceivedCas(carAheadSpeed);
                distanceSensor.publish();
                casSensorOverride.publish();
            }
            Thread.sleep(1000);
        }

        autoDrive.stopAutoDrive();

        ArgumentCaptor<Message> argumentCaptor = ArgumentCaptor.forClass(Message.class);
        verify(subscriber,atLeast(expectedInteractions-10)).receive(argumentCaptor.capture());

        List<Message> capturedMessages = argumentCaptor.getAllValues();

        double lastMeasuredDistance = -1;
        double lastMeasuredSpeed = -1;

        for (Message message : Lists.reverse(capturedMessages)) {
            if (message.type().equals("distance"))
                lastMeasuredDistance = Double.parseDouble(message.data());
            if (message.type().equals("speed"))
                lastMeasuredSpeed = Double.parseDouble(message.data());
            if(lastMeasuredDistance > 0 && lastMeasuredSpeed > 0)
                break;
        }

        System.out.println("Last measured distance: " + lastMeasuredDistance);
        System.out.println("Last measured speed: " + lastMeasuredSpeed);

        assertThat(lastMeasuredDistance,greaterThan(16.0));
        assertThat(lastMeasuredSpeed,closeTo(carAheadSpeed,0.5));
    }

    private double speedAtSeconds(double s) {
        double speed;
        return ((speed = AutoDrive.maxAcceleration * s) > speedLimit) ? speedLimit : speed;
    }
}
