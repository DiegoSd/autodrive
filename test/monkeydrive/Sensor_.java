package monkeydrive;

import monkeydrive.bus.BusZero;
import monkeydrive.sensor.BaseSensor;
import monkeydrive.sensor.BaseVirtualSensor;
import org.junit.Before;
import org.junit.Test;

import static utils.Utils.messageOfType;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class Sensor_ {

    private Sensor sensor;
    private VirtualSensor virtualSensor;

    private Bus bus;

    @Before
    public void setUp() {
        bus = new BusZero();

        sensor = new BaseSensor();

        virtualSensor = new BaseVirtualSensor();
    }

    @Test
    public void should_publish_correctly() {
        Subscriber subscriber = mock(Subscriber.class);

        bus.register(subscriber).to("sensor");

        sensor.associate(bus);

        sensor.publish();

        verify(subscriber,times(1)).receive(any());
    }

    @Test
    public void virtual_sensor_works() {
        Subscriber subscriber = mock(Subscriber.class);

        bus.register(subscriber).to("virtual-sensor");

        sensor.associate(bus);

        bus.register(virtualSensor).to("sensor");
        virtualSensor.associate(bus);

        sensor.publish();

        verify(subscriber,times(1)).receive(any());
    }

    @Test
    public void final_validation_test() {
        Subscriber subscriberA = mock(Subscriber.class);
        Subscriber subscriberB = mock(Subscriber.class);
        Subscriber subscriberC = mock(Subscriber.class);

        bus.register(subscriberA).to("sensor");
        bus.register(subscriberB).to("virtual-sensor");
        bus.register(subscriberC).to("sensor","virtual-sensor");

        sensor.associate(bus);

        bus.register(virtualSensor).to("sensor","foo","bar");
        virtualSensor.associate(bus);

        sensor.publish();
        bus.send(messageOfType("foo"));
        bus.send(messageOfType("foobar"));

        verify(subscriberA,times(1)).receive(any());
        verify(subscriberB,times(2)).receive(any());
        verify(subscriberC,times(3)).receive(any());
    }

}
