package utils;

import monkeydrive.Message;
import monkeydrive.message.CarAheadSpeedMessage;
import monkeydrive.sensor.CarAheadSpeedSensor;

public class CasSensorOverride extends CarAheadSpeedSensor {

    private double perceivedCas;

    public void updatePerceivedCas(double perceivedCas) {
        this.perceivedCas = perceivedCas;
    }

    @Override
    protected Message generateMessage() {
        return new CarAheadSpeedMessage(perceivedCas);
    }
}
