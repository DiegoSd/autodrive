package utils;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.Instant;

import static java.lang.StrictMath.abs;

public class TestingPhysics {

    private Instant lastUpdate = Instant.MIN;

    public void analyzeDistances() throws InterruptedException {

        double speed1 = 16;
        double speeds2[] = {16,16,16.5,17,17,17};
        double accelerations2[] = {0,0,1,1,0,0};

        double distanceBetween = 30;

        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);

        for(int i=0; i<6; i++) {
            double interval = updateInterval();

            double traversed1 = traversed(speed1, 0, interval);
            double traversed2 = traversed(speeds2[i], accelerations2[i], interval);

            Thread.sleep(500);

            distanceBetween += (traversed1 - traversed2);
            System.out.println("Distance traversed by car 1 is: " + traversed2);
            System.out.println("Distance between them is: " + df.format(distanceBetween));
        }
    }

    public void analyzeSpeeds() throws InterruptedException {


        double speeds1[]        = {16, 16,   16.5, 17,   17, 17};
        double accelerations1[] = { 0,  0,      1,  1,    0,  0};
        double distances[]      = {30, 30, 29.625, 29, 28.5, 28};

        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);

        for(int i=0; i<6; i++) {
            double interval = updateInterval();

            //distance between drop = distance traversed car 1 - distance traversed car 2
            // => d.t.c2 = d.t.c1 - d.b.d
            // and d.t.c2 = car 1 speed * t
            // => s.c1 = (d.t.c1 - d.b.d) / t

            double traversed1 = traversed(speeds1[i],accelerations1[i],interval);

            double distanceBetweenDrop;
            if(i == 0)
                distanceBetweenDrop = 0;
            else
                distanceBetweenDrop = distances[i-1] - distances[i];

            double speed2 = otherVehicleSpeed(traversed1,distanceBetweenDrop,0,interval);

            if(i==2 || i==3) {
                System.out.println("In " + interval + " seconds:");
                System.out.println("\tCar 1 traversed: " + traversed1);
                System.out.println("\tAccelerating at: " + accelerations1[i]);
                System.out.println("\tThe distance between them variation was: " + distanceBetweenDrop);
                System.out.println("\tCalculated speed: " + speed2);
            }

            Thread.sleep(500);
        }

    }

    public void analyzeSpeeds2() throws InterruptedException {

        double speed1 = 16.0;
        double speeds2[] = {20.0, 20.0, 19.5, 19.0, 18.5, 18.0, 17.5, 17.0};

        double distanceBetween = 50;

        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);

        for(int i=0; i<8; i++) {
            double interval = updateInterval();

            double traversed1 = traversed(speed1,0,interval);
            double traversed2;
            if(i == 0)
                traversed2 = traversed(speeds2[i],0,interval);
            else
                traversed2 = traversed(speeds2[i],(speeds2[i]-speeds2[i-1])/interval,interval);

            double previousDistance = distanceBetween;
            distanceBetween = distanceBetween - traversed1 + traversed2;

            System.out.println("In "+interval+" seconds:");
            System.out.println("\tDistance traversed by car 1 = "+traversed1);
            System.out.println("\tDistance traversed by car 2 = "+traversed2);
            System.out.println("\tDistance between them = "+distanceBetween);
            System.out.println("\t∆D.B. = "+(distanceBetween-previousDistance));

            Thread.sleep(250);
        }

    }

    public void analyzeSpeeds3() throws InterruptedException {

        double speed1 = 33.33;
        double speed2 = 20.0;

        double distanceBetween = 100;

        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);

        for(int i=0; i<50; i++) {
            double interval = updateInterval();

            double traversed1 = traversed(speed1,0,interval);
            double traversed2;
            if(i == 0)
                traversed2 = traversed(speed1,0,interval);
            else
                traversed2 = traversed(speed2,0,interval);

            double previousDistance = distanceBetween;
            distanceBetween = distanceBetween - traversed1 + traversed2;

            //System.out.println("In "+interval+" seconds:");
            //System.out.println("\tDistance traversed by car 1 = "+traversed1);
            //System.out.println("\tDistance traversed by car 2 = "+traversed2);
            System.out.println(i+":\tDistance between them = "+distanceBetween);
            //System.out.println("\t∆D.B. = "+(distanceBetween-previousDistance));

            Thread.sleep(50);
        }

    }

    private double updateInterval() {
        if(lastUpdate == Instant.MIN) {
            lastUpdate = Instant.now();
            return 0;
        }

        long interval = Instant.now().toEpochMilli() - lastUpdate.toEpochMilli();

        lastUpdate = Instant.now();

        return (1 / (double) 1000)*interval;
    }

    static double traversed(double initialSpeed, double acceleration, double interval) {
        return initialSpeed*interval + 0.5*acceleration*(interval*interval);
    }

    static double otherVehicleSpeed(double traversedDistance, double distanceBetweenDrop,
                                    double otherVehicleAcceleration, double interval) {
        return (traversedDistance + distanceBetweenDrop + 0.5*otherVehicleAcceleration*(interval*interval))
                / interval;
    }

    static double timeUntilCollision(double carBehindSpeed, double carAheadSpeed, double initialDistance) {
        return initialDistance / (carBehindSpeed - carAheadSpeed);
    }

    static double timeToReachSpeed(double initialSpeed, double desiredSpeed, double maxAcceleration) {
        return (desiredSpeed - initialSpeed) / maxAcceleration;
    }

    static double minAccelerationToReachSpeedBeforeCollision(double initialSpeed, double desiredSpeed, double initialDistance) {
        return (desiredSpeed - initialSpeed) / timeUntilCollision(initialSpeed,desiredSpeed,initialDistance);
    }

    static double securityDistance(double speed, double maxBreakDeceleration) {
        return - (speed*speed) / (2.0 * maxBreakDeceleration);
    }
}

