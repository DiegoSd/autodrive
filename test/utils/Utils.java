package utils;

import monkeydrive.Message;

import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class Utils {
    public static Message messageOfType(String type) {
        Message message = mock(Message.class, RETURNS_DEEP_STUBS);
        when(message.type()).thenReturn(type);
        return message;
    }

}
